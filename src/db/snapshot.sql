--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4 (Debian 13.4-1.pgdg100+1)
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sqlpatch; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sqlpatch;


ALTER SCHEMA sqlpatch OWNER TO postgres;

--
-- Name: measurement_partition_trg(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.measurement_partition_trg() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
declare
    year_text text;
    week_text text;
    week_next_text text;
    from_date date;
    to_date date;
begin

    if tg_op = 'INSERT' then

        year_text = lpad(new.year::text, 4, '0');
        week_text = lpad(new.week::text, 2, '0');
        week_next_text = lpad((new.week+1)::text, 2, '0');
        from_date = to_date(year_text || week_text, 'iyyyiw');
        to_date = to_date(year_text || week_next_text, 'iyyyiw');

        execute format('
create table public.measurement_y%1$sw%2$s(
    like public.measurement including all
);

create table public.measurement_label_y%1$sw%2$s(
    like public.measurement_label including all
);

create table public.sample_udp_data_y%1$sw%2$s(
    like public.sample_udp_data including all
);
', year_text, week_text);

        if new.attached then
            execute format('
alter table public.measurement
attach partition public.measurement_y%1$sw%2$s
for values from (%3$L) to (%4$L);

alter table public.measurement_label
attach partition public.measurement_label_y%1$sw%2$s
for values from (%3$L) to (%4$L);

alter table public.sample_udp_data
attach partition public.sample_udp_data_y%1$sw%2$s
for values from (%3$L) to (%4$L);
', year_text, week_text, from_date, to_date);
        end if;
    end if;

    if tg_op = 'DELETE' then
        year_text = lpad(old.year::text, 4, '0');
        week_text = lpad(old.week::text, 2, '0');

        execute format('
drop table public.sample_udp_data_y%1$sw%2$s cascade;
drop table public.measurement_label_y%1$sw%2$s cascade;
drop table public.measurement_y%1$sw%2$s cascade;
', year_text, week_text);
    end if;

    if tg_op = 'UPDATE' then
        if new.year != old.year then
            raise exception 'not allowed to change year';
        end if;

        if new.week != old.week then
            raise exception 'not allowed to change week';
        end if;

        year_text = lpad(new.year::text, 4, '0');
        week_text = lpad(new.week::text, 2, '0');
        week_next_text = lpad((new.week+1)::text, 2, '0');
        from_date = to_date(year_text || week_text, 'iyyyiw');
        to_date = to_date(year_text || week_next_text, 'iyyyiw');

        if new.attached and not old.attached then
            execute format('
alter table public.measurement
attach partition public.measurement_y%1$sw%2$s
for values from (%3$L) to (%4$L);

alter table public.measurement_label
attach partition public.measurement_label_y%1$sw%2$s
for values from (%3$L) to (%4$L);

alter table public.sample_udp_data
attach partition public.sample_udp_data_y%1$sw%2$s
for values from (%3$L) to (%4$L);
', year_text, week_text, from_date, to_date);
        end if;

        if not new.attached and old.attached then
            execute format('
alter table public.measurement
detach partition public.measurement_y%1$sw%2$s;

alter table public.measurement_label
detach partition public.measurement_label_y%1$sw%2$s;

alter table public.sample_udp_data
detach partition public.sample_udp_data_y%1$sw%2$s;
', year_text, week_text, from_date, to_date);
        end if;

    end if;

    return new;
end;
$_$;


ALTER FUNCTION public.measurement_partition_trg() OWNER TO postgres;

--
-- Name: prune_beacons(); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.prune_beacons()
    LANGUAGE plpgsql
    AS $$
begin

    delete from public.beacon
    where not id in (
        select beacon
        from public.sample_udp_data
    );

end;
$$;


ALTER PROCEDURE public.prune_beacons() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: beacon; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beacon (
    version character varying(20) NOT NULL,
    ipv4 inet NOT NULL,
    ipv6 inet,
    id bytea NOT NULL,
    client bytea NOT NULL,
    location bytea NOT NULL,
    created_utc timestamp without time zone NOT NULL,
    public_key bytea,
    last_seen_utc timestamp without time zone NOT NULL
);


ALTER TABLE public.beacon OWNER TO postgres;

--
-- Name: location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.location (
    id bytea NOT NULL,
    created_utc timestamp without time zone NOT NULL
);


ALTER TABLE public.location OWNER TO postgres;

--
-- Name: location_label; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.location_label (
    location bytea NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.location_label OWNER TO postgres;

--
-- Name: measurement; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
)
PARTITION BY RANGE (timestamp_utc);


ALTER TABLE public.measurement OWNER TO postgres;

--
-- Name: measurement_label; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
)
PARTITION BY RANGE (timestamp_utc);


ALTER TABLE public.measurement_label OWNER TO postgres;

--
-- Name: measurement_label_y2021w34; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w34 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w34 FOR VALUES FROM ('2021-08-23 00:00:00') TO ('2021-08-30 00:00:00');


ALTER TABLE public.measurement_label_y2021w34 OWNER TO postgres;

--
-- Name: measurement_label_y2021w35; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w35 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w35 FOR VALUES FROM ('2021-08-30 00:00:00') TO ('2021-09-06 00:00:00');


ALTER TABLE public.measurement_label_y2021w35 OWNER TO postgres;

--
-- Name: measurement_label_y2021w36; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w36 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w36 FOR VALUES FROM ('2021-09-06 00:00:00') TO ('2021-09-13 00:00:00');


ALTER TABLE public.measurement_label_y2021w36 OWNER TO postgres;

--
-- Name: measurement_label_y2021w37; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w37 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w37 FOR VALUES FROM ('2021-09-13 00:00:00') TO ('2021-09-20 00:00:00');


ALTER TABLE public.measurement_label_y2021w37 OWNER TO postgres;

--
-- Name: measurement_label_y2021w38; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w38 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w38 FOR VALUES FROM ('2021-09-20 00:00:00') TO ('2021-09-27 00:00:00');


ALTER TABLE public.measurement_label_y2021w38 OWNER TO postgres;

--
-- Name: measurement_label_y2021w39; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w39 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w39 FOR VALUES FROM ('2021-09-27 00:00:00') TO ('2021-10-04 00:00:00');


ALTER TABLE public.measurement_label_y2021w39 OWNER TO postgres;

--
-- Name: measurement_label_y2021w40; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w40 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w40 FOR VALUES FROM ('2021-10-04 00:00:00') TO ('2021-10-11 00:00:00');


ALTER TABLE public.measurement_label_y2021w40 OWNER TO postgres;

--
-- Name: measurement_label_y2021w41; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w41 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w41 FOR VALUES FROM ('2021-10-11 00:00:00') TO ('2021-10-18 00:00:00');


ALTER TABLE public.measurement_label_y2021w41 OWNER TO postgres;

--
-- Name: measurement_label_y2021w42; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w42 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w42 FOR VALUES FROM ('2021-10-18 00:00:00') TO ('2021-10-25 00:00:00');


ALTER TABLE public.measurement_label_y2021w42 OWNER TO postgres;

--
-- Name: measurement_label_y2021w43; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w43 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w43 FOR VALUES FROM ('2021-10-25 00:00:00') TO ('2021-11-01 00:00:00');


ALTER TABLE public.measurement_label_y2021w43 OWNER TO postgres;

--
-- Name: measurement_label_y2021w44; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w44 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w44 FOR VALUES FROM ('2021-11-01 00:00:00') TO ('2021-11-08 00:00:00');


ALTER TABLE public.measurement_label_y2021w44 OWNER TO postgres;

--
-- Name: measurement_label_y2021w45; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w45 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w45 FOR VALUES FROM ('2021-11-08 00:00:00') TO ('2021-11-15 00:00:00');


ALTER TABLE public.measurement_label_y2021w45 OWNER TO postgres;

--
-- Name: measurement_label_y2021w46; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w46 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w46 FOR VALUES FROM ('2021-11-15 00:00:00') TO ('2021-11-22 00:00:00');


ALTER TABLE public.measurement_label_y2021w46 OWNER TO postgres;

--
-- Name: measurement_label_y2021w47; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w47 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w47 FOR VALUES FROM ('2021-11-22 00:00:00') TO ('2021-11-29 00:00:00');


ALTER TABLE public.measurement_label_y2021w47 OWNER TO postgres;

--
-- Name: measurement_label_y2021w48; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w48 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w48 FOR VALUES FROM ('2021-11-29 00:00:00') TO ('2021-12-06 00:00:00');


ALTER TABLE public.measurement_label_y2021w48 OWNER TO postgres;

--
-- Name: measurement_label_y2021w49; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w49 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w49 FOR VALUES FROM ('2021-12-06 00:00:00') TO ('2021-12-13 00:00:00');


ALTER TABLE public.measurement_label_y2021w49 OWNER TO postgres;

--
-- Name: measurement_label_y2021w50; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w50 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w50 FOR VALUES FROM ('2021-12-13 00:00:00') TO ('2021-12-20 00:00:00');


ALTER TABLE public.measurement_label_y2021w50 OWNER TO postgres;

--
-- Name: measurement_label_y2021w51; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w51 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w51 FOR VALUES FROM ('2021-12-20 00:00:00') TO ('2021-12-27 00:00:00');


ALTER TABLE public.measurement_label_y2021w51 OWNER TO postgres;

--
-- Name: measurement_label_y2021w52; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_label_y2021w52 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    name text NOT NULL,
    value text NOT NULL
);
ALTER TABLE ONLY public.measurement_label ATTACH PARTITION public.measurement_label_y2021w52 FOR VALUES FROM ('2021-12-27 00:00:00') TO ('2022-01-03 00:00:00');


ALTER TABLE public.measurement_label_y2021w52 OWNER TO postgres;

--
-- Name: measurement_partition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_partition (
    year integer NOT NULL,
    week integer NOT NULL,
    attached boolean
);


ALTER TABLE public.measurement_partition OWNER TO postgres;

--
-- Name: measurement_y2021w34; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w34 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w34 FOR VALUES FROM ('2021-08-23 00:00:00') TO ('2021-08-30 00:00:00');


ALTER TABLE public.measurement_y2021w34 OWNER TO postgres;

--
-- Name: measurement_y2021w35; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w35 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w35 FOR VALUES FROM ('2021-08-30 00:00:00') TO ('2021-09-06 00:00:00');


ALTER TABLE public.measurement_y2021w35 OWNER TO postgres;

--
-- Name: measurement_y2021w36; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w36 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w36 FOR VALUES FROM ('2021-09-06 00:00:00') TO ('2021-09-13 00:00:00');


ALTER TABLE public.measurement_y2021w36 OWNER TO postgres;

--
-- Name: measurement_y2021w37; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w37 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w37 FOR VALUES FROM ('2021-09-13 00:00:00') TO ('2021-09-20 00:00:00');


ALTER TABLE public.measurement_y2021w37 OWNER TO postgres;

--
-- Name: measurement_y2021w38; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w38 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w38 FOR VALUES FROM ('2021-09-20 00:00:00') TO ('2021-09-27 00:00:00');


ALTER TABLE public.measurement_y2021w38 OWNER TO postgres;

--
-- Name: measurement_y2021w39; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w39 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w39 FOR VALUES FROM ('2021-09-27 00:00:00') TO ('2021-10-04 00:00:00');


ALTER TABLE public.measurement_y2021w39 OWNER TO postgres;

--
-- Name: measurement_y2021w40; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w40 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w40 FOR VALUES FROM ('2021-10-04 00:00:00') TO ('2021-10-11 00:00:00');


ALTER TABLE public.measurement_y2021w40 OWNER TO postgres;

--
-- Name: measurement_y2021w41; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w41 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w41 FOR VALUES FROM ('2021-10-11 00:00:00') TO ('2021-10-18 00:00:00');


ALTER TABLE public.measurement_y2021w41 OWNER TO postgres;

--
-- Name: measurement_y2021w42; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w42 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w42 FOR VALUES FROM ('2021-10-18 00:00:00') TO ('2021-10-25 00:00:00');


ALTER TABLE public.measurement_y2021w42 OWNER TO postgres;

--
-- Name: measurement_y2021w43; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w43 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w43 FOR VALUES FROM ('2021-10-25 00:00:00') TO ('2021-11-01 00:00:00');


ALTER TABLE public.measurement_y2021w43 OWNER TO postgres;

--
-- Name: measurement_y2021w44; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w44 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w44 FOR VALUES FROM ('2021-11-01 00:00:00') TO ('2021-11-08 00:00:00');


ALTER TABLE public.measurement_y2021w44 OWNER TO postgres;

--
-- Name: measurement_y2021w45; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w45 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w45 FOR VALUES FROM ('2021-11-08 00:00:00') TO ('2021-11-15 00:00:00');


ALTER TABLE public.measurement_y2021w45 OWNER TO postgres;

--
-- Name: measurement_y2021w46; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w46 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w46 FOR VALUES FROM ('2021-11-15 00:00:00') TO ('2021-11-22 00:00:00');


ALTER TABLE public.measurement_y2021w46 OWNER TO postgres;

--
-- Name: measurement_y2021w47; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w47 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w47 FOR VALUES FROM ('2021-11-22 00:00:00') TO ('2021-11-29 00:00:00');


ALTER TABLE public.measurement_y2021w47 OWNER TO postgres;

--
-- Name: measurement_y2021w48; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w48 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w48 FOR VALUES FROM ('2021-11-29 00:00:00') TO ('2021-12-06 00:00:00');


ALTER TABLE public.measurement_y2021w48 OWNER TO postgres;

--
-- Name: measurement_y2021w49; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w49 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w49 FOR VALUES FROM ('2021-12-06 00:00:00') TO ('2021-12-13 00:00:00');


ALTER TABLE public.measurement_y2021w49 OWNER TO postgres;

--
-- Name: measurement_y2021w50; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w50 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w50 FOR VALUES FROM ('2021-12-13 00:00:00') TO ('2021-12-20 00:00:00');


ALTER TABLE public.measurement_y2021w50 OWNER TO postgres;

--
-- Name: measurement_y2021w51; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w51 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w51 FOR VALUES FROM ('2021-12-20 00:00:00') TO ('2021-12-27 00:00:00');


ALTER TABLE public.measurement_y2021w51 OWNER TO postgres;

--
-- Name: measurement_y2021w52; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.measurement_y2021w52 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    client bytea NOT NULL
);
ALTER TABLE ONLY public.measurement ATTACH PARTITION public.measurement_y2021w52 FOR VALUES FROM ('2021-12-27 00:00:00') TO ('2022-01-03 00:00:00');


ALTER TABLE public.measurement_y2021w52 OWNER TO postgres;

--
-- Name: sample_udp_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
)
PARTITION BY RANGE (timestamp_utc);


ALTER TABLE public.sample_udp_data OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w34; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w34 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w34 FOR VALUES FROM ('2021-08-23 00:00:00') TO ('2021-08-30 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w34 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w35; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w35 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w35 FOR VALUES FROM ('2021-08-30 00:00:00') TO ('2021-09-06 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w35 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w36; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w36 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w36 FOR VALUES FROM ('2021-09-06 00:00:00') TO ('2021-09-13 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w36 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w37; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w37 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w37 FOR VALUES FROM ('2021-09-13 00:00:00') TO ('2021-09-20 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w37 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w38; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w38 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w38 FOR VALUES FROM ('2021-09-20 00:00:00') TO ('2021-09-27 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w38 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w39; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w39 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w39 FOR VALUES FROM ('2021-09-27 00:00:00') TO ('2021-10-04 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w39 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w40; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w40 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w40 FOR VALUES FROM ('2021-10-04 00:00:00') TO ('2021-10-11 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w40 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w41; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w41 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w41 FOR VALUES FROM ('2021-10-11 00:00:00') TO ('2021-10-18 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w41 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w42; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w42 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w42 FOR VALUES FROM ('2021-10-18 00:00:00') TO ('2021-10-25 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w42 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w43; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w43 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w43 FOR VALUES FROM ('2021-10-25 00:00:00') TO ('2021-11-01 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w43 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w44; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w44 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w44 FOR VALUES FROM ('2021-11-01 00:00:00') TO ('2021-11-08 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w44 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w45; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w45 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w45 FOR VALUES FROM ('2021-11-08 00:00:00') TO ('2021-11-15 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w45 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w46; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w46 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w46 FOR VALUES FROM ('2021-11-15 00:00:00') TO ('2021-11-22 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w46 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w47; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w47 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w47 FOR VALUES FROM ('2021-11-22 00:00:00') TO ('2021-11-29 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w47 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w48; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w48 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w48 FOR VALUES FROM ('2021-11-29 00:00:00') TO ('2021-12-06 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w48 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w49; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w49 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w49 FOR VALUES FROM ('2021-12-06 00:00:00') TO ('2021-12-13 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w49 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w50; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w50 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w50 FOR VALUES FROM ('2021-12-13 00:00:00') TO ('2021-12-20 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w50 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w51; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w51 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w51 FOR VALUES FROM ('2021-12-20 00:00:00') TO ('2021-12-27 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w51 OWNER TO postgres;

--
-- Name: sample_udp_data_y2021w52; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sample_udp_data_y2021w52 (
    timestamp_utc timestamp without time zone NOT NULL,
    source inet NOT NULL,
    beacon bytea NOT NULL,
    raw json,
    rtt_ms integer NOT NULL,
    stddev numeric NOT NULL
);
ALTER TABLE ONLY public.sample_udp_data ATTACH PARTITION public.sample_udp_data_y2021w52 FOR VALUES FROM ('2021-12-27 00:00:00') TO ('2022-01-03 00:00:00');


ALTER TABLE public.sample_udp_data_y2021w52 OWNER TO postgres;

--
-- Name: patch; Type: TABLE; Schema: sqlpatch; Owner: postgres
--

CREATE TABLE sqlpatch.patch (
    name character varying(100) NOT NULL,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    checksum character(40)
);


ALTER TABLE sqlpatch.patch OWNER TO postgres;

--
-- Data for Name: beacon; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: location_label; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w34; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w35; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w36; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w37; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w38; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w39; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w40; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w41; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w42; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w43; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w44; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w45; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w46; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w47; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w48; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w49; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w50; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w51; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_label_y2021w52; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_partition; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 34, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 35, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 36, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 37, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 38, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 39, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 40, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 41, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 42, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 43, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 44, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 45, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 46, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 47, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 48, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 49, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 50, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 51, true);
INSERT INTO public.measurement_partition (year, week, attached) VALUES (2021, 52, true);


--
-- Data for Name: measurement_y2021w34; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w35; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w36; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w37; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w38; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w39; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w40; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w41; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w42; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w43; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w44; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w45; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w46; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w47; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w48; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w49; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w50; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w51; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: measurement_y2021w52; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w34; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w35; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w36; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w37; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w38; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w39; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w40; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w41; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w42; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w43; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w44; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w45; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w46; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w47; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w48; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w49; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w50; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w51; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: sample_udp_data_y2021w52; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: patch; Type: TABLE DATA; Schema: sqlpatch; Owner: postgres
--

INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('initial_table', '2021-07-27 17:31:55.215106', '7108d0ddfe9ae5ac50b1921c91468ab194c6724e');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('initial_table_pk', '2021-07-27 17:31:55.217506', '66f7bcc043799958da16f617abcce0a03fd3d8b0');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('beacon', '2021-07-31 22:07:33.865861', '54991e99b20fc8c94e7a8fa2115feece40c37e69');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('measurement_type_table', '2021-07-31 22:07:33.876093', '34f8c0d1cd45ba769ae0313a82a0abc285d2565c');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('client_table', '2021-07-31 22:07:33.880719', '37084ee4006e9c1e2945d10f65a7ad4e9c06c956');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('measurement_table', '2021-07-31 22:07:33.884229', '8e0fb2ff08e1f7b02a76f61396ea4bd8374be809');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('measurement_table1', '2021-07-31 22:07:33.891257', '66d54ed94766fd080aa6dfdf966a13ab95d1d4a5');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('beacon_ip', '2021-07-31 22:07:33.892777', 'd21af3bb9f812f25d13230d2c5beeac94790bc58');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('beacon_id', '2021-07-31 22:07:33.893894', 'efce4aee945de2e11046447b48cadf93428d6c09');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('initial_table_drop', '2021-07-31 22:07:33.898873', '447cfdda35c1d5c384665030c200aac68150d077');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('measurement_type_table1', '2021-07-31 22:07:33.900705', '1a5b13e06d4881387a906a113bd02fa13003a1e1');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('measurement_table2', '2021-07-31 22:08:07.539269', 'c0d6dc26f79ba24b0ef548217136e642e60acdcc');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('api-key', '2021-11-20 09:30:21.503549', '7b70f58d105278edc307f63d9ba260a5f02d1004');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('beacon-client', '2021-11-20 09:30:21.506767', '51240cbfbd70ced667819da724ee577b5050c613');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('drop-beacon-client-default', '2021-11-20 09:30:21.510567', 'cbdfae7b9936b028d85ab4dd1984cf7af959637e');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('last-seen', '2021-11-20 09:30:21.511471', '32ec76ed21cd83c37094e7333b0f38fb91ee2529');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('version-as-string', '2021-11-20 09:30:21.512493', 'bfb0e209ab561046b68abd69613a27c465ee74e6');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('longer-version', '2021-11-20 09:30:21.518703', '7e463b740b7b06304bffefa06c023839f65213de');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('noop', '2021-11-20 09:30:21.519791', 'dc3274987681ec98f88e306232262e6b7dd5372c');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('test-client', '2021-11-20 09:30:21.520489', 'a5ad3746f78326ea7faee9afc8c0418b6484bd50');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('fix_measurement_timestamp', '2021-12-20 12:23:33.944499', 'af5db0ebe8e94c27baf4a6cc572f5bf162fb887a');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('client-id', '2021-12-20 12:23:33.947231', '2c83a79a862ebb9216b9c5dcb240a18407fbb011');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('sample_udp_data', '2021-12-20 12:23:33.967333', '83d54e84e647f836f60f0955fa33a7c2170f9dab');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('measurement_label', '2021-12-20 12:23:33.975737', '614d851eb55ae1a801e6d4e76c280b9bcfc3629f');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('partition', '2021-12-20 12:23:33.980289', 'c745ab722433bb2470aa0e888fe2a07eba03d53f');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('beacon_label', '2021-12-20 12:23:34.197608', '562af2e3ce8defbd399efd516cea86db9a4d94c2');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('location', '2021-12-20 12:23:34.204831', '6ae97ca7d433757d500641fced98c6f419b6aa60');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('cleanup', '2021-12-20 12:23:34.21458', '3ca71782ff9225d36a721e4890bedab6126119fd');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('beacon_id-2', '2021-12-20 12:23:34.216878', 'd6f1b8f4b58694afc320b4f4499b5fc2a68d26d4');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('beacon_public_key', '2021-12-20 12:23:34.296727', '6818cc7c6e4e47f37195c12eda8ea7ea363600c3');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('client-secret', '2021-12-20 12:23:34.298308', '77642c962d9fac327089db09984438ae706917b1');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('drop-client', '2021-12-20 12:23:34.303423', '4d7c0ec14f2a25fbf0127739c873e128228008bf');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('drop_measurement_type', '2021-12-20 12:23:34.307958', '8847286337dc0ffacbe20acebc6621298febe5dc');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('last-seen-is-back', '2021-12-20 12:23:34.309281', 'c0fb5a8690dd347593ef02bf8258201f234fa8e9');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('last-seen-not-null', '2021-12-20 12:23:34.311576', '1a1aa5e5fa76859f8e68313ae27e5d17c9a1d111');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('prune_beacons', '2021-12-20 12:23:34.312447', '39c1257108be321bf4564d4c84e406d0e5d70a83');


--
-- Name: beacon beacon_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beacon
    ADD CONSTRAINT beacon_pk PRIMARY KEY (id);


--
-- Name: location_label location_label_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location_label
    ADD CONSTRAINT location_label_pk PRIMARY KEY (location, name);


--
-- Name: location location_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_pk PRIMARY KEY (id);


--
-- Name: measurement_partition measurement_partition_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_partition
    ADD CONSTRAINT measurement_partition_pk PRIMARY KEY (year, week);


--
-- Name: measurement measurement_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement
    ADD CONSTRAINT measurement_pk PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w34 measurement_y2021w34_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w34
    ADD CONSTRAINT measurement_y2021w34_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w35 measurement_y2021w35_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w35
    ADD CONSTRAINT measurement_y2021w35_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w36 measurement_y2021w36_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w36
    ADD CONSTRAINT measurement_y2021w36_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w37 measurement_y2021w37_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w37
    ADD CONSTRAINT measurement_y2021w37_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w38 measurement_y2021w38_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w38
    ADD CONSTRAINT measurement_y2021w38_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w39 measurement_y2021w39_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w39
    ADD CONSTRAINT measurement_y2021w39_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w40 measurement_y2021w40_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w40
    ADD CONSTRAINT measurement_y2021w40_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w41 measurement_y2021w41_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w41
    ADD CONSTRAINT measurement_y2021w41_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w42 measurement_y2021w42_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w42
    ADD CONSTRAINT measurement_y2021w42_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w43 measurement_y2021w43_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w43
    ADD CONSTRAINT measurement_y2021w43_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w44 measurement_y2021w44_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w44
    ADD CONSTRAINT measurement_y2021w44_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w45 measurement_y2021w45_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w45
    ADD CONSTRAINT measurement_y2021w45_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w46 measurement_y2021w46_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w46
    ADD CONSTRAINT measurement_y2021w46_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w47 measurement_y2021w47_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w47
    ADD CONSTRAINT measurement_y2021w47_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w48 measurement_y2021w48_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w48
    ADD CONSTRAINT measurement_y2021w48_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w49 measurement_y2021w49_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w49
    ADD CONSTRAINT measurement_y2021w49_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w50 measurement_y2021w50_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w50
    ADD CONSTRAINT measurement_y2021w50_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w51 measurement_y2021w51_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w51
    ADD CONSTRAINT measurement_y2021w51_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: measurement_y2021w52 measurement_y2021w52_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.measurement_y2021w52
    ADD CONSTRAINT measurement_y2021w52_pkey PRIMARY KEY (timestamp_utc, source);


--
-- Name: sample_udp_data sample_udp_data_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data
    ADD CONSTRAINT sample_udp_data_pk PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w34 sample_udp_data_y2021w34_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w34
    ADD CONSTRAINT sample_udp_data_y2021w34_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w35 sample_udp_data_y2021w35_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w35
    ADD CONSTRAINT sample_udp_data_y2021w35_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w36 sample_udp_data_y2021w36_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w36
    ADD CONSTRAINT sample_udp_data_y2021w36_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w37 sample_udp_data_y2021w37_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w37
    ADD CONSTRAINT sample_udp_data_y2021w37_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w38 sample_udp_data_y2021w38_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w38
    ADD CONSTRAINT sample_udp_data_y2021w38_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w39 sample_udp_data_y2021w39_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w39
    ADD CONSTRAINT sample_udp_data_y2021w39_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w40 sample_udp_data_y2021w40_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w40
    ADD CONSTRAINT sample_udp_data_y2021w40_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w41 sample_udp_data_y2021w41_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w41
    ADD CONSTRAINT sample_udp_data_y2021w41_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w42 sample_udp_data_y2021w42_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w42
    ADD CONSTRAINT sample_udp_data_y2021w42_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w43 sample_udp_data_y2021w43_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w43
    ADD CONSTRAINT sample_udp_data_y2021w43_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w44 sample_udp_data_y2021w44_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w44
    ADD CONSTRAINT sample_udp_data_y2021w44_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w45 sample_udp_data_y2021w45_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w45
    ADD CONSTRAINT sample_udp_data_y2021w45_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w46 sample_udp_data_y2021w46_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w46
    ADD CONSTRAINT sample_udp_data_y2021w46_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w47 sample_udp_data_y2021w47_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w47
    ADD CONSTRAINT sample_udp_data_y2021w47_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w48 sample_udp_data_y2021w48_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w48
    ADD CONSTRAINT sample_udp_data_y2021w48_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w49 sample_udp_data_y2021w49_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w49
    ADD CONSTRAINT sample_udp_data_y2021w49_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w50 sample_udp_data_y2021w50_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w50
    ADD CONSTRAINT sample_udp_data_y2021w50_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w51 sample_udp_data_y2021w51_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w51
    ADD CONSTRAINT sample_udp_data_y2021w51_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: sample_udp_data_y2021w52 sample_udp_data_y2021w52_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sample_udp_data_y2021w52
    ADD CONSTRAINT sample_udp_data_y2021w52_pkey PRIMARY KEY (timestamp_utc, source, beacon);


--
-- Name: patch patch_pkey; Type: CONSTRAINT; Schema: sqlpatch; Owner: postgres
--

ALTER TABLE ONLY sqlpatch.patch
    ADD CONSTRAINT patch_pkey PRIMARY KEY (name);


--
-- Name: measurement_y2021w34_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w34_pkey;


--
-- Name: measurement_y2021w35_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w35_pkey;


--
-- Name: measurement_y2021w36_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w36_pkey;


--
-- Name: measurement_y2021w37_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w37_pkey;


--
-- Name: measurement_y2021w38_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w38_pkey;


--
-- Name: measurement_y2021w39_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w39_pkey;


--
-- Name: measurement_y2021w40_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w40_pkey;


--
-- Name: measurement_y2021w41_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w41_pkey;


--
-- Name: measurement_y2021w42_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w42_pkey;


--
-- Name: measurement_y2021w43_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w43_pkey;


--
-- Name: measurement_y2021w44_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w44_pkey;


--
-- Name: measurement_y2021w45_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w45_pkey;


--
-- Name: measurement_y2021w46_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w46_pkey;


--
-- Name: measurement_y2021w47_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w47_pkey;


--
-- Name: measurement_y2021w48_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w48_pkey;


--
-- Name: measurement_y2021w49_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w49_pkey;


--
-- Name: measurement_y2021w50_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w50_pkey;


--
-- Name: measurement_y2021w51_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w51_pkey;


--
-- Name: measurement_y2021w52_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.measurement_pk ATTACH PARTITION public.measurement_y2021w52_pkey;


--
-- Name: sample_udp_data_y2021w34_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w34_pkey;


--
-- Name: sample_udp_data_y2021w35_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w35_pkey;


--
-- Name: sample_udp_data_y2021w36_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w36_pkey;


--
-- Name: sample_udp_data_y2021w37_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w37_pkey;


--
-- Name: sample_udp_data_y2021w38_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w38_pkey;


--
-- Name: sample_udp_data_y2021w39_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w39_pkey;


--
-- Name: sample_udp_data_y2021w40_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w40_pkey;


--
-- Name: sample_udp_data_y2021w41_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w41_pkey;


--
-- Name: sample_udp_data_y2021w42_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w42_pkey;


--
-- Name: sample_udp_data_y2021w43_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w43_pkey;


--
-- Name: sample_udp_data_y2021w44_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w44_pkey;


--
-- Name: sample_udp_data_y2021w45_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w45_pkey;


--
-- Name: sample_udp_data_y2021w46_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w46_pkey;


--
-- Name: sample_udp_data_y2021w47_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w47_pkey;


--
-- Name: sample_udp_data_y2021w48_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w48_pkey;


--
-- Name: sample_udp_data_y2021w49_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w49_pkey;


--
-- Name: sample_udp_data_y2021w50_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w50_pkey;


--
-- Name: sample_udp_data_y2021w51_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w51_pkey;


--
-- Name: sample_udp_data_y2021w52_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.sample_udp_data_pk ATTACH PARTITION public.sample_udp_data_y2021w52_pkey;


--
-- Name: measurement_partition measurement_partition_trg; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER measurement_partition_trg AFTER INSERT OR DELETE OR UPDATE ON public.measurement_partition FOR EACH ROW EXECUTE FUNCTION public.measurement_partition_trg();


--
-- Name: beacon beacon_location_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beacon
    ADD CONSTRAINT beacon_location_fk FOREIGN KEY (location) REFERENCES public.location(id);


--
-- Name: location_label location_location_label_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location_label
    ADD CONSTRAINT location_location_label_fk FOREIGN KEY (location) REFERENCES public.location(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: measurement_label measurement_measurement_label_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE public.measurement_label
    ADD CONSTRAINT measurement_measurement_label_fk FOREIGN KEY (timestamp_utc, source) REFERENCES public.measurement(timestamp_utc, source) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sample_udp_data sample_udp_data_beacon_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE public.sample_udp_data
    ADD CONSTRAINT sample_udp_data_beacon_fk FOREIGN KEY (beacon) REFERENCES public.beacon(id);


--
-- PostgreSQL database dump complete
--

